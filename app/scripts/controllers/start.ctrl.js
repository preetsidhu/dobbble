(function () {
  'use strict';

  angular.module('dobbleApp')
    .controller('StartCtrl', startCtrl)

  startCtrl.$inject = ['$state', '$rootScope']

  function startCtrl($state, $rootScope) {
    var start = this

    start.startGame = startGame
    start.toggleLevels = toggleLevels

    function toggleLevels() {
      start.showLevels = true
    }

    function startGame(levelNum) {
      // initialization logic
      $rootScope.gameLevel = levelNum
      $state.go('game')
    }
  }
})();
