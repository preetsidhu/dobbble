(function () {
  'use strict';

  angular.module('dobbleApp')
    .factory('HelperService', helperService)

  helperService.$inject = ['CARD_SET', 'IMAGE_SET']

  function helperService(CARD_SET, IMAGE_SET) {
    return {
      createDeck: createDeck,
      createIconSet: createIconSet
    }

    function createDeck(numCards) {
      return shuffleDeck(shuffleEachCard(getDeck(numCards)))
    }

    function createIconSet(setName) {
      return appendLocationPath(shuffle(getIconSet(setName)), setName)
    }

    function appendLocationPath(imgSet, imgSetName) {
      return imgSet.map(function (img) {
        return '../images/' + imgSetName + '/' + img
      })
    }

    function getIconSet(name) {
      switch (name) {
      case IMAGE_SET.SET1_NAME:
        return IMAGE_SET.SET1
        break;
      case IMAGE_SET.SET2_NAME:
        return IMAGE_SET.SET2
        break;
      case IMAGE_SET.SET3_NAME:
        return IMAGE_SET.SET3
        break;
      default:
        return 'nothing'
        break;
      }
    }

    function getDeck(number) {
      switch (number) {
      case CARD_SET.SET3_NUM:
        return CARD_SET.SET3
        break;
      case CARD_SET.SET2_NUM:
        return CARD_SET.SET2
        break;
      case CARD_SET.SET1_NUM:
        return CARD_SET.SET1
        break;
      default:
        return 'nothing'
        break;
      }
    }

    function shuffleEachCard(cardSet) {
      return cardSet.map(function (card) {
        return shuffle(card)
      })
    }

    function shuffleDeck(cardSet) {
      return shuffle(cardSet)
    }

    function shuffle(array) {
      var currentIndex = array.length,
        temporaryValue, randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }


    // incomplete algo
    //    function createDeck (p) {
    //      var cards = []
    //      for (min=0; min < range(2, 1+parseInt(Math.pow(p, 0.5))); min++) {
    //        if (p % min_factor == 0) {
    //          break
    //        } else {
    //          min = p
    //        }
    //      }
    //
    //      for () {
    //
    //      }
    //
    //
    //    }

    //    function set (arr) {
    //      return arr.reduce(function (a, val) {
    //        if (a.indexOf(val) === -1) {
    //          a.push(val);
    //        }
    //        return a;
    //      }, []);
    //    }
    //
    //    function range(start, edge, step) {
    //      // If only one number was passed in make it the edge and 0 the start.
    //      if (arguments.length == 1) {
    //        edge = start;
    //        start = 0;
    //      }
    //
    //      // Validate the edge and step numbers.
    //      edge = edge || 0;
    //      step = step || 1;
    //
    //      // Create the array of numbers, stopping befor the edge.
    //      for (var ret = []; (edge - start) * step > 0; start += step) {
    //        ret.push(start);
    //      }
    //      return ret;
    //    }
  }
})();
