(function () {
  'use strict';

  angular.module('dobbleApp')
    .controller('GameCtrl', gameCtrl)

  gameCtrl.$inject = ['$scope', '$rootScope', 'HelperService', 'IMAGE_SET', 'CARD_SET']

  function gameCtrl($scope, $rootScope, HelperService, IMAGE_SET, CARD_SET) {
    var game = this,
      cardIndex = 0,
      imageSetIndex = {
        '3': IMAGE_SET.SET1_NAME,
        '5': IMAGE_SET.SET2_NAME,
        '7': IMAGE_SET.SET3_NAME
      },
      transformStyles = [],
      sizeStyles = [],
      isFirstCard = true


    game.level = $rootScope.gameLevel || '3'
    game.isMatch = isMatch
    game.setTableStyle = setTableStyle
    game.setHandStyle = setHandStyle

    init()

    function init() {
      game.currDeck = HelperService.createDeck(game.level)
      game.prevCard = game.currDeck[cardIndex]
      game.currCard = getNextCard(cardIndex)
      game.imageSet = HelperService.createIconSet(imageSetIndex[game.level])
    }

    function isMatch(index) {
      game.prevCard.indexOf(index) > -1 ? matchSuccess() : matchFailure()
    }

    function matchSuccess() {
      matchSuccessHandler()
    }

    function matchFailure() {
      matchFailureHandler()
    }

    //    $scope.$on('Match: Success', matchSuccessHandler())

    //    $scope.$on('Match: Failure', matchFailureHandler())

    function matchSuccessHandler() {
      game.currCard = getNextCard()
    }

    function getNextCard() {
      if (cardIndex > 0) game.prevCard = game.currCard
      cardIndex = cardIndex + 1
      return game.currDeck[cardIndex]
    }

    function matchFailureHandler() {
      //      alertFailure()
    }
      
    function setTableStyle(i){
      if(isFirstCard){
          var size, sizeValue;
          if(game.level === '3'){
              size = (Math.random() * 8) + 12;
              size = parseFloat(Math.round(size * 100)/100).toFixed(1);
          } else if(game.level === '5'){
              size = (Math.random() * 5) + 10;
              size = parseFloat(Math.round(size * 100)/100).toFixed(1);
          } else {
              size = (Math.random() * 3) + 8;
              size = parseFloat(Math.round(size * 100)/100).toFixed(1);
          }
          sizeValue = size + '%';
          console.log('width: ' + sizeValue);
          var rotation = Math.random() * 35;
          rotation = parseFloat(Math.round(rotation * 100)/100).toFixed(1);
          rotation *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
          var rotationValue = 'rotate(' + rotation + 'deg)';
          if(i == parseInt(game.level)){
              isFirstCard = !isFirstCard;
          }
          
          return {
              'width': sizeValue,
              'transform': rotationValue
          };
      } else {
          console.log('called?' + JSON.stringify(sizeStyles));
          return {
              'width': sizeStyles[i],
              'transform': transformStyles[i]
          };
      }
    }
      
    function setHandStyle(i){
        var size, sizeValue;
        if(game.level === '3'){
            size = (Math.random() * 8) + 12;
            size = parseFloat(Math.round(size * 100)/100).toFixed(1);
        } else if(game.level === '5'){
            size = (Math.random() * 5) + 10;
            size = parseFloat(Math.round(size * 100)/100).toFixed(1);
        } else {
            size = (Math.random() * 3) + 8;
            size = parseFloat(Math.round(size * 100)/100).toFixed(1);
        }
        sizeValue = size + '%';
        console.log('width2: ' + sizeValue);
        sizeStyles[i] = sizeValue;
        var rotation = Math.random() * 35;
        rotation = parseFloat(Math.round(rotation * 100)/100).toFixed(1);
        rotation *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
        var rotationValue = 'rotate(' + rotation + 'deg)';
        transformStyles[i] = rotationValue;
        return {
            'width': sizeValue,
            'transform': rotationValue
        };
    }
  }
})();
