angular
  .module('dobbleApp', [
    'ui.router',
  ])
  .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider
      .otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/start.tmpl.html',
        controller: 'StartCtrl',
        controllerAs: 'start'
      })
      .state('game', {
        url: '/start-game',
        templateUrl: 'views/game.tmpl.html',
        controller: 'GameCtrl',
        controllerAs: 'game'
      })
  }]);
